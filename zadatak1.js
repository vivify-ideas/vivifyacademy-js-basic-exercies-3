/* 1. Napisati konstruktor (funkciju) Macka, koja sadrži atribute (property-e) ime, majka i otac.  
    Konstruktor prima tri argumenta, ime, majku i oca. Ukoliko jedan ili oba roditelja nisu prosleđeni, inicijalne vrednosti treba da budu stringovi “nepoznata” i “nepoznat”.
    Dodati (u konstruktoru, na this) metod mjau koji treba samo da ispiše u konzoli “<ime> kaze mjau”.
    Napraviti objekat klase Macka, sa imenom Cvrle i nepoznatim roditeljima
*/

function Macka(ime, majka, otac) {
  this.ime = ime;
  this.majka = majka || 'nepoznato';
  this.otac = otac || 'nepoznato';

  this.mjau = function () {
    console.log(this.ime + ' kaze mjau');
  }
}

var cvrle = new Macka('cvrle');
